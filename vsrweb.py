from flask import Flask, render_template, request, escape
from vsr import search4letters
from time import sleep
from threading import Thread

from DBcm import UseDatabase, ConnectionError, CredentialsError


app = Flask(__name__)

app.config["dbconfig"] = {'host': '127.0.0.1', 
                'user': 'shadevillson', 
                'password': '020510121212', 
                'database': 'vsrlogDB',}
def log_request(req: 'flask_request', res: str) -> None:
    """Запись подробностей веб-запроса и его результатов в логе"""

    with UseDatabase(app.config["dbconfig"]) as cursor:
        _SQL = """insert into log
                (phrase, letters, ip, browser_string, results)
                values
                (%s, %s, %s, %s, %s)"""

        cursor.execute(_SQL, (req.form['phrase'],
                        req.form['letters'],
                        req.remote_addr,
                        req.user_agent.string,
                        res, ))

#with open('vsr.log', 'a') as log: 
#        print(req.form, req.remote_addr, req.user_agent, res, file=log, sep='|')


@app.route('/search4', methods=['POST'])
def do_search() -> html: 

    @copy_current_request_context
    def log_request(req: 'flask_request', res: str) -> None:
        sleep(15)
        with UseDatabase(app.config['dbconfig']) as cursor:
            _SQL = """insert into log
                    (phrase, letters, ip, browser_string, results)
                    values
                      (%s, %s, %s, %s, %s)"""
            cursor.execute(_SQL, (req.form['phrase'],
                                req.form['letters'], 
                                req.remote_addr, 
                                req.user_agent.browser, 
                                res, ))
    phrase = request.form['phrase'] 
    letters = request.form['letters']
    title = "Ваши результаты:"
    results = str(search4letters(phrase, letters))
    try:
        t = Thread(target=log_request, args=(request, results))
        t.start()
    except Exception as err:
        print("***** Logging failed with this error:", str(err))
    return render_template('results.html',
                            the_phrase=phrase,
                            the_letters=letters,
                            the_title=title,
                            the_results=results,)

@app.route('/')
@app.route('/entry') 
def entry_page() -> 'html': 
    return render_template('entry.html', 
        the_title='Welcome to search4letters on the web!')

@app.route('/viewlog') 
def view_the_log() -> 'html': 
    try: 
        with UseDatabase(app.config["dbconfig"]) as cursor:
            _SQL = """select phrase, letters, ip, browser_string, results 
                        from log"""
            cursor.execute(_SQL)
            contents = cursor.fetchall()
        titles = ('Phrase','Letters', 'Remote_addr', 'User_agent', 'Results') 
        return render_template('viewlog.html', 
                            the_title='View Log', 
                            the_row_titles=titles, 
                            the_data=contents,)
    except ConnectionError as err:
        print('Включена ли ваша база данных? Ошибка:', str(err))
    except CredentialsError as err: 
        print('Проблемы с идентификатором пользователя/паролем. Ошибка:', str(err))
    except SQLError as err:
        print('Is your query correct? Error:', str(err))
    except Exception as err:
        print('Что-то пошло не так:', str(err))

if __name__ == '__main__':
    app.run(debug=True)